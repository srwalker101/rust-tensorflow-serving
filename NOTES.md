# Notes

# Todo

* write the following test:

- start the model server with only version 1
- query model version 1 specifically (assert true)
- query model version 2 specifically (assert failure)
- copy model version 1 to version 2
- query model version 1 specifically (assert true)
- query model version 2 specifically (assert failure)
- reload config
- query model version 1 specifically (assert true)
- query model version 2 specifically (assert true)
