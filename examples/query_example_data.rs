use std::path::PathBuf;
use structopt::StructOpt;
use tensorflow_serving::TensorflowServing;

#[derive(StructOpt, Debug)]
struct Opts {
    #[structopt(parse(from_os_str))]
    image: PathBuf,
    #[structopt(short = "m", long = "model")]
    model: String,
    #[structopt(long = "version")]
    model_version: Option<i64>,
    #[structopt(long = "hostname", default_value = "127.0.0.1")]
    hostname: String,
    #[structopt(long = "port", default_value = "9000")]
    port: u16,
}

fn main() {
    let opts = Opts::from_args();

    let img = image::open(opts.image).expect("reading image");

    let serving = TensorflowServing::new()
        .hostname(opts.hostname)
        .port(opts.port)
        .build()
        .unwrap();

    let model_definition = tensorflow_serving::ModelDescription {
        name: opts.model,
        version: opts.model_version,
    };

    let prediction = serving
        .predict_with_preprocessing(img, model_definition, |value| value / 255.)
        .unwrap();
    println!("{:?}", prediction);
}
