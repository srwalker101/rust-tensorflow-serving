use failure::Error;
use std::collections::HashMap;
use std::path::PathBuf;
use tensorflow_serving::TensorflowServing;

fn main() -> Result<(), Error> {
    let serving = TensorflowServing::new()
        .hostname("127.0.0.1")
        .port(9000)
        .build()?;

    let mut new_config = HashMap::new();
    new_config.insert(
        "resnet".to_string(),
        PathBuf::from("/models/resnet".to_string()),
    );

    let response = serving.reload_config(new_config)?;
    println!("Got response: {:?}", response);

    Ok(())
}
