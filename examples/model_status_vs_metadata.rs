use tensorflow_serving::TensorflowServing;
use failure::Error;

fn main() -> Result<(), Error> {
    let serving = TensorflowServing::new()
        .hostname("127.0.0.1")
        .port(9000)
        .build()?;

    let metadata = serving.get_model_metadata("resnet")?;
    println!("{:?}", metadata);

    let status = serving.get_model_status("resnet")?;
    println!("{:?}", status);


    Ok(())
}
