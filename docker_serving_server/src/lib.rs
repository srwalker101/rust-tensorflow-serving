use lazy_static::lazy_static;
use std::fs;
use std::path::Path;
use std::process::{Child, Command, Stdio};
use std::thread;
use std::time::Duration;

lazy_static! {
    static ref CONTAINER_NAME: String = {
        let uid = uuid::Uuid::new_v4();
        format!("tensorflow-serving-{}", uid.to_hyphenated().to_string())
    };
}

pub struct DockerServingServer {
    handle: Child,
}

impl DockerServingServer {
    pub fn new<P>(path: P, port: u16) -> Self
    where
        P: AsRef<Path>,
    {
        Self::clear_any_running();

        let handle = Self::spawn_new_server(path, port);

        Self { handle }
    }

    fn clear_any_running() {
        let mut child = Command::new("docker")
            .args(&["rm", "-f", CONTAINER_NAME.as_str()])
            .stderr(Stdio::null())
            .spawn()
            .expect("spawning docker clear command");
        child.wait().expect("waiting for docker clear command");
    }

    fn spawn_new_server<P>(path: P, port: u16) -> Child
    where
        P: AsRef<Path>,
    {
        let full_path = fs::canonicalize(path).expect("fetching full path for model dir");
        let models_mount_str = format!("{}:/models/resnet", full_path.display());
        let child = Command::new("docker")
            .args(&[
                "run",
                "-p",
                format!("{}:8500", port).as_str(),
                "-v",
                models_mount_str.as_str(),
                "-e",
                "MODEL_NAME=resnet",
                "-t",
                "--name",
                CONTAINER_NAME.as_str(),
                "tensorflow/serving:latest",
            ])
            .stdout(Stdio::null())
            .stderr(Stdio::null())
            .spawn()
            .expect("spawning docker run process");

        thread::sleep(Duration::from_secs(1));
        child
    }
}

impl Drop for DockerServingServer {
    fn drop(&mut self) {
        DockerServingServer::clear_any_running();
        self.handle.wait().expect("waiting for docker process");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn check_running() -> bool {
        let output = Command::new("docker").args(&["ps"]).output().unwrap();
        let stdout = std::str::from_utf8(&output.stdout).unwrap();
        for line in stdout.lines() {
            let line = line.trim();
            if line.is_empty() || line.contains("CONTAINER") {
                continue;
            }

            if line.contains(CONTAINER_NAME.as_str()) {
                return true;
            }
        }

        false
    }

    fn check_not_running() -> bool {
        !check_running()
    }

    #[test]
    fn server_spawning() {
        let model_path = Path::new(file!())
            .parent()
            .unwrap()
            .join("..")
            .join("..")
            .join("serving")
            .join("models");

        {
            let _server = DockerServingServer::new(model_path, 9000);
            assert!(check_running());
        }

        assert!(check_not_running());
    }
}
