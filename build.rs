fn main() {
    let proto_root = "protos";
    let output = "src";
    let protos = vec![
"tensorflow_serving/sources/storage_path/file_system_storage_path_source.proto",
"tensorflow_serving/apis/inference.proto",
"tensorflow_serving/apis/model.proto",
"tensorflow_serving/apis/prediction_service.proto",
"tensorflow_serving/apis/classification.proto",
"tensorflow_serving/apis/get_model_metadata.proto",
"tensorflow_serving/apis/get_model_status.proto",
"tensorflow_serving/apis/regression.proto",
"tensorflow_serving/apis/input.proto",
"tensorflow_serving/apis/model_management.proto",
"tensorflow_serving/apis/predict.proto",
"tensorflow_serving/apis/model_service.proto",
"tensorflow_serving/config/logging_config.proto",
"tensorflow_serving/config/log_collector_config.proto",
"tensorflow_serving/config/model_server_config.proto",
"tensorflow_serving/util/status.proto",
"tensorflow/core/example/feature.proto",
"tensorflow/core/example/example.proto",
"tensorflow/core/lib/core/error_codes.proto",
"tensorflow/core/framework/graph.proto",
"tensorflow/core/framework/function.proto",
"tensorflow/core/framework/op_def.proto",
"tensorflow/core/framework/attr_value.proto",
"tensorflow/core/framework/types.proto",
"tensorflow/core/framework/resource_handle.proto",
"tensorflow/core/framework/tensor_shape.proto",
"tensorflow/core/framework/tensor.proto",
"tensorflow/core/framework/node_def.proto",
"tensorflow/core/framework/versions.proto",
"tensorflow/core/protobuf/struct.proto",
"tensorflow/core/protobuf/trackable_object_graph.proto",
"tensorflow/core/protobuf/meta_graph.proto",
"tensorflow/core/protobuf/saved_object_graph.proto",
"tensorflow/core/protobuf/saver.proto",
    ];
    println!("cargo-rerun-if-changed={}", proto_root);
    protoc_grpcio::compile_grpc_protos(
        &protos,
        &[proto_root],
        &output,
        None
    ).expect("Failed to compile gRPC definitions!");
}
