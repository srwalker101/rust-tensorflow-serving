#![deny(missing_docs)]

/*! This package integrates [Tensorflow Serving][tensorflow-serving] with Rust.

```rust,no_run
# use tensorflow_serving::TensorflowServing;
# use failure::Error;
let serving = TensorflowServing::new()
    .hostname("127.0.0.1")
    .port(8500)
    .build()?;

// Your test image is stored at `img_path`, or it is an already open `image::DynamicImage`
# let img_path = "foobar";
let prediction = serving.predict(img_path, "resnet")?;

println!("Probabilities: {:?}", prediction.probabilities);
println!("Top index: {}", prediction.max_idx);
# Ok::<(), Error>(())
```

[tensorflow-serving]: https://www.tensorflow.org/tfx/guide/serving
*/


// Load all of the modules up. These have been generated via the `protoc` compiler
mod attr_value;
mod classification;
mod error_codes;
mod example;
mod feature;
mod file_system_storage_path_source;
mod function;
mod get_model_metadata;
mod get_model_status;
mod graph;
mod inference;
mod input;
mod log_collector_config;
mod logging_config;
mod meta_graph;
mod model_management;
mod model;
mod model_server_config;
mod model_service_grpc;
mod model_service;
mod node_def;
mod op_def;
mod prediction_service_grpc;
mod prediction_service;
mod predict;
mod regression;
mod resource_handle;
mod saved_object_graph;
mod saver;
mod status;
mod struct_pb;
mod tensor;
mod tensor_shape;
mod trackable_object_graph;
mod types;
mod versions;

use failure::{bail, format_err, Error, ResultExt};
use grpcio::*;
use image::{DynamicImage, GenericImageView};
use std::collections::HashMap;
use std::iter::FromIterator;
use std::path::{Path, PathBuf};
use std::sync::Arc;

use crate::model::ModelSpec;
use crate::predict::{PredictRequest, PredictResponse};
use crate::tensor::TensorProto;
use crate::tensor_shape::{TensorShapeProto, TensorShapeProto_Dim};
use crate::model_service_grpc::ModelServiceClient;
use crate::prediction_service_grpc::PredictionServiceClient;
use crate::types::DataType;

/// Our custom result type
pub type Result<T> = std::result::Result<T, Error>;

/// Trait representing either an image or some image data
///
pub trait Image {
    /// Extract an image from the enclosed object
    ///
    fn to_image(&self) -> Result<DynamicImage>;
}

impl Image for dyn AsRef<Path> {
    fn to_image(&self) -> Result<DynamicImage> {
        image::open(self)
            .context("reading image from disk")
            .map_err(From::from)
    }
}

impl Image for DynamicImage {
    fn to_image(&self) -> Result<DynamicImage> {
        Ok(self.clone())
    }
}

impl Image for PathBuf {
    fn to_image(&self) -> Result<DynamicImage> {
        image::open(self)
            .context("reading image from disk")
            .map_err(From::from)
    }
}

impl Image for &str {
    fn to_image(&self) -> Result<DynamicImage> {
        image::open(self)
            .context("reading image from disk")
            .map_err(From::from)
    }
}

/// Clssification payload
///
/// Can be either bytes, ints, or floats
pub enum Payload {
    /// Bytes payload
    Bytes(Vec<Vec<u8>>),
    /// Ints payload
    Ints(Vec<i64>),
    /// Floats payload
    Floats(Vec<f32>),
}

impl From<Vec<Vec<u8>>> for Payload {
    fn from(v: Vec<Vec<u8>>) -> Self {
        Payload::Bytes(v)
    }
}

impl From<Vec<i64>> for Payload {
    fn from(v: Vec<i64>) -> Self {
        Payload::Ints(v)
    }
}

impl From<Vec<f32>> for Payload {
    fn from(v: Vec<f32>) -> Self {
        Payload::Floats(v)
    }
}

impl From<Payload> for feature::Feature {
    fn from(c: Payload) -> Self {
        let data_list = match c {
            Payload::Bytes(v) => {
                let data_list = feature::BytesList {
                    value: v.into(),
                    ..Default::default()
                };

                feature::Feature_oneof_kind::bytes_list(data_list)
            }
            Payload::Ints(v) => {
                let data_list = feature::Int64List {
                    value: v.into(),
                    ..Default::default()
                };

                feature::Feature_oneof_kind::int64_list(data_list)
            }
            Payload::Floats(v) => {
                let data_list = feature::FloatList {
                    value: v.into(),
                    ..Default::default()
                };

                feature::Feature_oneof_kind::float_list(data_list)
            }
        };

        feature::Feature {
            kind: Some(data_list),
            ..Default::default()
        }
    }
}

/// Builder pattern used to build the client.
///
/// This struct is created by calling `TensorflowServing::new()`. It represents a partially
/// configured client. Use the builder pattern to construct a client gradually. Then call the
/// `build` method to construct a concrete `TensorflowServing` instance.
///
/// Required parameters are:
///
/// * hostname
/// * port
/// * model_name
///
/// `signature_name` is optional, and defaults to "serving_default".
///
#[derive(Default)]
pub struct TensorflowServingBuilder {
    hostname: Option<String>,
    port: Option<u16>,
    signature_name: Option<String>,
}

impl TensorflowServingBuilder {
    /// Set the hostname for the client
    ///
    pub fn hostname<S: Into<String>>(&mut self, hostname: S) -> &mut Self {
        self.hostname = Some(hostname.into());
        self
    }

    /// Set the port for the client
    ///
    pub fn port(&mut self, port: u16) -> &mut Self {
        self.port = Some(port);
        self
    }

    /// Set the signature name
    ///
    pub fn signature_name<S: Into<String>>(&mut self, signature_name: S) -> &mut Self {
        self.signature_name = Some(signature_name.into());
        self
    }

    /// Build a `TensorflowServing` client.
    ///
    pub fn build(&mut self) -> Result<TensorflowServing> {
        if let None = self.hostname {
            bail!("hostname not provided");
        }

        if let None = self.port {
            bail!("port not provided");
        }

        let signature_name = self
            .signature_name
            .take()
            .unwrap_or_else(|| "serving_default".to_string());

        let hostname = self.hostname.take().unwrap();

        // TODO: this many threads?
        let env = Arc::new(Environment::new(8));
        let prediction_channel = ChannelBuilder::new(env.clone()).connect(&format!(
            "{}:{}",
            hostname,
            self.port.unwrap()
        ));
        let prediction_client = PredictionServiceClient::new(prediction_channel);

        let model_management_channel = ChannelBuilder::new(env.clone()).connect(&format!(
            "{}:{}",
            hostname,
            self.port.unwrap()
        ));
        let model_management_client = ModelServiceClient::new(model_management_channel);

        Ok(TensorflowServing {
            prediction_client,
            model_management_client,
            signature_name: signature_name,
        })
    }
}

/// Tensorflow Serving client
///
/// Used to talk to a Tensorflow Serving server.
///
pub struct TensorflowServing {
    prediction_client: PredictionServiceClient,
    model_management_client: ModelServiceClient,
    signature_name: String,
}

trait MapToFeatures {
    fn to_features(self) -> feature::Features;
}

impl<S, V> MapToFeatures for HashMap<S, V>
where
    S: Into<String>,
    V: Into<Payload>,
{
    fn to_features(self) -> feature::Features {
        let i = self.into_iter().map(|(k, v)| {
            let payload = v.into();
            let mut feature = feature::Feature::new();

            match payload {
                Payload::Bytes(v) => {
                    let bytes_list = feature.mut_bytes_list();
                    bytes_list.set_value(v.into());
                }
                Payload::Ints(v) => {
                    let int64_list = feature.mut_int64_list();
                    int64_list.set_value(v.into());
                }
                Payload::Floats(v) => {
                    let float_list = feature.mut_float_list();
                    float_list.set_value(v.into());
                }
            }

            (k.into(), feature)
        });
        feature::Features {
            feature: HashMap::from_iter(i),
            ..Default::default()
        }
    }
}

impl From<HashMap<String, PathBuf>> for model_server_config::ModelConfigList {
    fn from(h: HashMap<String, PathBuf>) -> Self {
        let configs = h
            .iter()
            .map(|(name, path)| model_server_config::ModelConfig {
                name: name.clone(),
                base_path: path.to_string_lossy().into_owned(),
                model_platform: "tensorflow".to_string(),
                ..Default::default()
            })
            .collect::<Vec<_>>();

        model_server_config::ModelConfigList {
            config: configs.into(),
            ..Default::default()
        }
    }
}

/// Description of a model
///
/// This struct is used to specify a model, and optionally a version of a model. It
/// implements From<Into<String>> so a `str` or `String` can be used to specify a model name
/// without worrying about a version number:
///
/// ```rust
/// # use tensorflow_serving::ModelDescription;
/// let model_name = "resnet";
/// let description: ModelDescription<_> = model_name.into();
/// ```
#[derive(Debug, Default)]
pub struct ModelDescription<S>
where
    S: Into<String>,
{
    /// Name of the model
    pub name: S,
    /// Optional version of the model
    pub version: Option<i64>,
}

impl<S> From<S> for ModelDescription<S>
where
    S: Into<String>,
{
    fn from(s: S) -> Self {
        ModelDescription {
            name: s,
            version: None,
        }
    }
}

impl TensorflowServing {
    /// Construct a new `TensorflowServing` builder struct.
    ///
    pub fn new() -> TensorflowServingBuilder {
        TensorflowServingBuilder::default()
    }

    /// Run a classification on a supplied image
    ///
    pub fn classify<S, T, F, V>(
        &self,
        model_name: S,
        payload_map: HashMap<T, V>,
    ) -> Result<classification::ClassificationResult>
    where
        S: Into<ModelDescription<F>>,
        F: Into<String>,
        T: Into<String>,
        V: Into<Payload>,
    {
        let req = classification::ClassificationRequest {
            model_spec: Some(self.build_model_spec(model_name)).into(),
            input: Some(self.build_input(payload_map)).into(),
            ..Default::default()
        };

        let resp = self.prediction_client.classify(&req)?;
        // TODO: remove this unwrap
        Ok(resp.result.unwrap())
    }

    /// Run a regression job
    pub fn regress<S, T, F, V>(
        &self,
        model_name: S,
        payload_map: HashMap<T, V>,
    ) -> Result<regression::RegressionResult>
    where
        S: Into<ModelDescription<F>>,
        F: Into<String>,
        T: Into<String>,
        V: Into<Payload>,
    {
        let req = regression::RegressionRequest {
            model_spec: Some(self.build_model_spec(model_name)).into(),
            input: Some(self.build_input(payload_map)).into(),
            ..Default::default()
        };

        let resp = self.prediction_client.regress(&req)?;
        Ok(resp.result.unwrap())
    }

    /// Run a prediction for a supplied image
    ///
    /// Supply something that implements `Into<Image>` i.e. either a path to an image file, or
    /// an already open `image::DynamicImage`, and a [`ModelDescription`][model-description],
    /// to get a prediction from the server.
    ///
    /// The `preprocessing_fn` parameter allows customisation of the pixel values.
    ///
    /// [model-description]: struct.ModelDescription.html
    pub fn predict_with_preprocessing<I, F, S, M>(
        &self,
        img: I,
        model_description: S,
        preprocessing_fn: M,
    ) -> Result<PredictionResult>
    where
        I: Image,
        F: Into<String>,
        S: Into<ModelDescription<F>>,
        M: Fn(f32) -> f32,
    {
        // Load data
        let img = img.to_image()?;

        let (width, height) = img.dimensions();
        let dims: Vec<_> = [1, width as i64, height as i64, 3]
            .iter()
            .map(|d| {
                let mut dim = TensorShapeProto_Dim::new();
                dim.set_size(*d);
                dim
            })
            .collect();

        let pixels: Vec<_> = img
            .raw_pixels()
            .iter()
            .map(|p| *p as f32)
            .map(preprocessing_fn)
            .collect();

        let tensor_shape = TensorShapeProto {
            dim: dims.into(),
            ..Default::default()
        };

        let tensor = TensorProto {
            dtype: DataType::DT_FLOAT,
            tensor_shape: Some(tensor_shape).into(),
            float_val: pixels,
            ..Default::default()
        };

        let mut inputs = HashMap::new();
        inputs.insert("input".into(), tensor);

        let request = PredictRequest {
            model_spec: Some(self.build_model_spec(model_description)).into(),
            inputs,
            ..Default::default()
        };

        let resp = self.prediction_client.predict(&request)?;
        PredictionResult::from_raw(resp)
    }

    /// Run a prediction (see [predict-with-preprocessing](struct.TensorflowServing.html#method.predict_with_preprocessing))
    pub fn predict<I, F, S>(&self, img: I, model_description: S) -> Result<PredictionResult>
    where
        I: Image,
        F: Into<String>,
        S: Into<ModelDescription<F>>,
    {
        self.predict_with_preprocessing(img, model_description, |p| p)
    }

    /// Perform multi-inference
    pub fn multi_inference<S, V>(
        &self,
        _model_name: S,
        _payload_map: HashMap<S, V>,
    ) -> Result<inference::MultiInferenceResponse>
    where
        S: Into<String>,
        V: Into<Payload>,
    {
        unimplemented!();
        /*
        let request = inference::MultiInferenceRequest {
            // TODO: tasks
            input: Some(self.build_input(payload_map)).into(),
            ..Default::default()
        };

        self.prediction_client
            .multi_inference(&request)
            .context("sending request to server")
            .map_err(From::from)
        */
    }

    /// Get model metadata
    pub fn get_model_metadata<S, T>(
        &self,
        model_name: S,
    ) -> Result<get_model_metadata::GetModelMetadataResponse>
    where
        T: Into<String>,
        S: Into<ModelDescription<T>>,
    {
        let request = get_model_metadata::GetModelMetadataRequest {
            model_spec: Some(self.build_model_spec(model_name)).into(),
            metadata_field: vec!["signature_def".to_string()].into(),
            ..Default::default()
        };

        self.prediction_client
            .get_model_metadata(&request)
            .context("sending request to server")
            .map_err(From::from)
    }

    /// Get model status
    pub fn get_model_status<S, T>(
        &self,
        model_name: S,
    ) -> Result<get_model_status::GetModelStatusResponse>
    where
        T: Into<String>,
        S: Into<ModelDescription<T>>,
    {
        let request = get_model_status::GetModelStatusRequest {
            model_spec: Some(self.build_model_spec(model_name)).into(),
            ..Default::default()
        };

        self.model_management_client
            .get_model_status(&request)
            .context("sending request to server")
            .map_err(From::from)
    }

    /// Reload the model configs
    pub fn reload_config<H>(&self, model_map: H) -> Result<model_management::ReloadConfigResponse>
    where
        H: Into<model_server_config::ModelConfigList>,
    {
        // request -> modelserverconfig -> ModelConfigList
        //
        let config_list = model_map.into();
        let model_server_config = model_server_config::ModelServerConfig {
            config: Some(
                model_server_config::ModelServerConfig_oneof_config::model_config_list(config_list),
            ),
            ..Default::default()
        };

        let request = model_management::ReloadConfigRequest {
            config: Some(model_server_config).into(),
            ..Default::default()
        };

        self.model_management_client
            .handle_reload_config_request(&request)
            .context("sending request to server")
            .map_err(From::from)
    }

    // Private helper functions
    fn build_input<S, V>(&self, payload_map: HashMap<S, V>) -> input::Input
    where
        S: Into<String>,
        V: Into<Payload>,
    {
        // Build Feature
        let ft = payload_map.to_features();

        // Build Vec<Example>
        let example = example::Example {
            features: Some(ft).into(),
            ..Default::default()
        };
        // Build ExampleList
        let example_list = input::ExampleList {
            examples: vec![example].into(),
            ..Default::default()
        };
        // Build Input
        let input = input::Input {
            kind: Some(input::Input_oneof_kind::example_list(example_list)).into(),
            ..Default::default()
        };

        input
    }

    fn build_model_spec<S, T>(&self, model_description: S) -> ModelSpec
    where
        S: Into<ModelDescription<T>>,
        T: Into<String>,
    {
        let desc = model_description.into();

        let version = desc.version.map(|version_id| {
            model::ModelSpec_oneof_version_choice::version(protobuf::well_known_types::Int64Value {
                value: version_id,
                ..Default::default()
            })
        });

        ModelSpec {
            name: desc.name.into(),
            version_choice: version,
            signature_name: self.signature_name.clone(),
            ..Default::default()
        }
    }
}

/// Result of prediction
#[derive(Debug)]
pub struct PredictionResult {
    /// Probability vector one per class
    ///
    pub probabilities: Vec<f32>,

    /// Index into the probability vector of the most likely class
    ///
    pub max_idx: i64,
}

impl PredictionResult {
    fn from_raw(response: PredictResponse) -> Result<Self> {
        let outputs = response.get_outputs();
        let probs = outputs
            .get("probabilities")
            .ok_or_else(|| format_err!("probabilities not available from the server response"))?;

        // Read the classes information
        let classes = outputs
            .get("classes")
            .ok_or_else(|| format_err!("classes not available from the server response"))?;

        if classes.dtype != DataType::DT_INT64 {
            bail!(
                "classes has unexpected data type, should be i64, got {:?}",
                classes.dtype
            );
        }

        let classes_shape = classes.get_tensor_shape();
        let dims = classes_shape.get_dim();

        if dims.len() != 1 {
            bail!("number of classes unexpected");
        }

        let dim = &dims[0];
        let n = dim.size;
        if n != 1 {
            bail!("number of classes unexpected");
        }

        let max_idx = classes.get_int64_val()[0];

        Ok(PredictionResult {
            probabilities: probs.float_val.clone(),
            max_idx,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_with_real_server() {
        let port = 9000;
        let img_path = "data/example.jpg";
        let serving = TensorflowServing::new()
            .hostname("127.0.0.1")
            .port(port)
            .build()
            .unwrap();

        let prediction = serving.predict(img_path, "resnet").unwrap();
        assert_eq!(prediction.max_idx, 905);
    }
}
